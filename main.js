
require('./local.settings');
require('./src/HttpControllers/BaseController');
const express = require('express');
const fs = require('fs');

// require the cron job scheduler
require('./src/CronJobs/Scheduler');

run();

async function run()
{
    const app = express();
    app.use(express.json());

    // wanted to have a dynamic way to add routes without having to modify main.js
    const filepaths = fs.readdirSync('./src/Routes');
    for (const filepath of filepaths)
    {
        const router = require(`./src/Routes/${filepath}`);
        app.use(router);
        if (process.argv.includes('--routes'))
        {
            printRoutes(filepath, router.stack);
        }
    }

    app.all('*', (req, res) => res.status(404).send('this endpoint does not exist.'));

    app.listen(process.env.PORT, async (err) =>
    {
        if (err) console.log('there is an error lol');
        console.log('listening on port ', process.env.PORT);
    });
}

/**
 * Prints out the routes that are available from this application
 * @param {String} filepath
 * @param {Object[]} routes
 */
function printRoutes(filepath, routes)
{
    console.log('\x1b[1m\x1b[3m\x1b[31m%s\x1b[0m', `${filepath.split('.')[0]}`);
    for (const route of routes)
    {
        const methods = Object.keys(route.route.methods);
        for (const method of methods)
        {
            console.log('\x1b[32m%s\x1b[0m\x1b[36m%s\x1b[0m', `- ${method.toUpperCase()}`, ` ${route.route.path.replace(/\([^)]+?\)/g, '')}`);
        }
    }
}
