const crypto = require('crypto');
const alg = 'aes-256-ctr';

class SymmetricEncryptor
{
    static encrypt(data, key, { iv, salt })
    {
        // convert hex string to buffer
        const ivBuffer = Buffer.from(iv, 'hex');
        const saltBuffer = Buffer.from(salt, 'hex');

        // generate key from password
        const keyBuffer = crypto.pbkdf2Sync(key, saltBuffer, 100000, 256 / 8, 'sha256');

        const cipher = crypto.createCipheriv(alg, keyBuffer, ivBuffer);

        // encrypt data
        let encrypted = cipher.update(data, 'utf8', 'hex');

        encrypted += cipher.final('hex');

        return encrypted;
    }

    static decrypt(encrypted, key, { iv, salt })
    {
        // convert hex string to buffer
        const ivBuffer = Buffer.from(iv, 'hex');
        const saltBuffer = Buffer.from(salt, 'hex');

        // generate keyBuffer from user secret Key
        const keyBuffer = crypto.pbkdf2Sync(key, saltBuffer, 100000, 256 / 8, 'sha256');

        const decipher = crypto.createDecipheriv(alg, keyBuffer, ivBuffer);

        const decrypted = Buffer.concat([decipher.update(Buffer.from(encrypted, 'hex')), decipher.final()]);

        const decString = decrypted.toString();

        if (decString.includes('�'))
            throw { 'status': 401, 'data': 'Invalid Key' };

        return decString;
    }
}

module.exports = SymmetricEncryptor;