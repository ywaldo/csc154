const PBJ = require('paillier-bigint')

class HomoMorphicEncryptor
{
    static add(encrypted, num, publicKey)
    {
        if (!publicKey)
            throw new Error('publicKey is required')

        const val = publicKey.addition(BigInt(encrypted), num)

        return val;
    }

    static multiply(encrypted, num, publicKey)
    {
        if (!publicKey)
            throw new Error('publicKey is required')

        if (typeof num !== 'number')
            throw new Error('num must be a number')

        return publicKey.multiply(BigInt(encrypted), BigInt(num))
    }

    static restoreKeys(lambda, mu, n, g)
    {
        const publicKey = HomoMorphicEncryptor.restorePublicKey(n, g);

        const privateKey = HomoMorphicEncryptor.restorePrivateKey(lambda, mu, publicKey);

        return { publicKey, privateKey };
    }

    static restorePrivateKey(lambda, mu, publicKey)
    {
        if (!publicKey)
            throw new Error('publicKey is required');

        if (!lambda)
            throw new Error('lambda is required');

        if (!mu)
            throw new Error('mu is required');

        lambda = BigInt(lambda)
        mu = BigInt(mu)

        return new PBJ.PrivateKey(lambda, mu, publicKey);
    }

    static restorePublicKey(n, g)
    {
        if (!n)
            throw new Error('n is required')

        if (!g)
            throw new Error('g is required')

        n = BigInt(n)
        g = BigInt(g)

        return new PBJ.PublicKey(n, g)
    }

    static encrypt(publicKey, num)
    {
        // check if num is a number
        if (typeof num !== 'number')
            throw new Error('num must be a number')

        // convert num to BigInt and encrypt
        return publicKey.encrypt(BigInt(num)).toString()
    }

    static decrypt(privateKey, content)
    {
        return parseInt(privateKey.decrypt(BigInt(content)))
    }

    static generateKeyPair(bitLength = 3072)
    {
        const { publicKey, privateKey } = PBJ.generateRandomKeysSync(bitLength);

        return { publicKey, privateKey }
    }
}

module.exports = HomoMorphicEncryptor;