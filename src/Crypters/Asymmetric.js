const crypto = require('crypto');

const defaultGuid = '00000000-0000-0000-0000-000000000000';

class AsymmetricEncryptor
{
    static generateKeyPair(guid = defaultGuid, keySize = 4096)
    {
        const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
            modulusLength: keySize,
            publicKeyEncoding: {
                type: 'spki',
                format: 'pem'
            },
            privateKeyEncoding: {
                type: 'pkcs8',
                format: 'pem',
                cipher: 'aes-256-cbc',
                passphrase: guid
            }
        });

        return { publicKey, privateKey };
    }

    static encrypt(data, publicKey)
    {
        const encrypted = crypto.publicEncrypt(publicKey, Buffer.from(data));

        return encrypted.toString('base64');
    }

    static decrypt(encrypted, privateKey, userGuid = defaultGuid)
    {
        const decrypted = crypto.privateDecrypt({ key: privateKey, passphrase: userGuid }, Buffer.from(encrypted, 'base64'));

        return decrypted.toString();
    }
}

module.exports = AsymmetricEncryptor;