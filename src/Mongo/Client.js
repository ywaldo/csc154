const { MongoClient } = require('mongodb');

const dbUrl = process.env['mongo.connection.uri'] || process.env.MONGO_CONNECTION_URI;
const dbName = process.env['mongo.database'] || process.env.MONGO_DATABASE;

const dbOptions =
{
    useUnifiedTopology: false,
    useNewUrlParser: true
};

let db;

/**
 * YOU MUST CALL "Mongo.connect()" before using anything that involves using Mongodb
 * Mongo only needs to have 1 connection open and mutex is used to enforce that.
 * Lock is locked at the beginning and will block any and all operations from trying to access the Mongo connection
 * before the connection is established. Only way to unlock it is by calling "Mongo.connect()"
 */
class Mongo
{
    static async connect()
    {
        if (!db)
        {
            const client = new MongoClient(dbUrl, dbOptions);
            db = (await client.connect()).db(dbName);
        }
        return db;
    }

    static async upsert(collection, filter, data)
    {
        const db = await Mongo.connect();

        await db.collection(collection).updateOne(filter, { $set: data }, { upsert: true });
    }

    static async insert(collection, data)
    {
        const db = await Mongo.connect();

        await db.collection(collection).insertOne(data);
    }

    static async delete(collection, filter)
    {
        const db = await Mongo.connect();

        await db.collection(collection).deleteOne(filter);
    }

    static async findOne(collection, filter, projections = {})
    {
        Object.assign(projections, { _id: 0 });

        const db = await Mongo.connect();

        const res = await db.collection(collection).findOne(filter, { projection: projections });

        return res;
    }

    static async findAll(collection, filter, projections)
    {
        const db = await Mongo.connect();

        const res = await db.collection(collection).find(filter).project(projections).toArray();

        return res;
    }

    static async getSecret(query)
    {
        if (typeof query === 'string')
            query = { name: query };

        const db = await Mongo.connect();

        const res = await db.collection('secrets').findOne(query, { projection: { _id: 0 } });

        return res;
    }

    static async updateSecret(key, data)
    {
        const db = await Mongo.connect();

        await db.collection('secrets').updateOne({ 'name': key }, { $set: data }, { upsert: true });
    }
}

module.exports = Mongo;