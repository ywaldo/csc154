const CryptService = require('../Services/CryptService');

class CryptController
{
    static async homomorphicAddition(req, res)
    {
        if (!req.file)
            res.status(400).json({ error: 'Missing File' });
        else
        {
            const data = await CryptService.homomorphicAddition(req.params.fileGuid, req.file, req.session.guid);

            res.status(200).json(data);
        }
    }

    static async homomorphicMultiplication(req, res)
    {
        if (!req.file)
            res.status(400).json({ error: 'Missing File' });
        else
        {
            const data = await CryptService.homomorphicMultiplication(req.params.fileGuid, req.file, req.session.guid);

            res.status(200).json(data);
        }
    }

    static async encryptHomomorphic(req, res)
    {
        if (!req.file)
            res.status(400).json({ error: 'Missing File' });
        else
        {
            const results = await CryptService.homomorphicEncrypt(req.file, req.session.guid);

            if (results)
                res.status(200).json(results);
        }
    }

    static async decryptHomomorphic(req, res)
    {
        if (!req.headers.privatekey)
            res.status(400).json({ error: 'Missing Private Key' });
        else
        {
            const result = await CryptService.homomorphicDecrypt(req.params.fileGuid, req.headers.privatekey, req.session.guid);

            if (result)
                res.status(200).send(result);
        }
    }

    static async encryptAsymmetric(req, res)
    {
        // we don't need key since we have public Key on user object
        if (!req.file)
            res.status(400).json({ error: 'Missing File' });
        else
        {
            // encrypt file
            const results = await CryptService.asymmetricEncrypt(req.file, req.session.guid);

            if (results)
                res.status(200).json(results);
        }
    }

    static async decryptAsymmetric(req, res)
    {
        if (!req.headers.privatekey)
            res.status(400).json({ error: 'Missing Private Key' });
        else
        {
            // convert \n to actual new line
            const privateKey = req.headers.privatekey.replace(/\\n/g, '\n');

            // get file from database
            const file = await CryptService.asymmetricDecrypt(req.params.fileGuid, privateKey, req.session.guid);

            if (file)
                res.status(200).send(file);
        }
    }

    static async encryptSymmetric(req, res)
    {
        // make sure file and key are provided
        if (!req.file || !req?.headers?.privatekey)
            res.status(400).json({ error: 'Missing File or Private Key' });
        else
        {
            // encrypt file
            const results = await CryptService.symmetricEncrypt(req.file, req.headers.privatekey, req.session.guid);

            if (results)
                res.status(200).json(results);
        }
    }

    static async decryptSymmetric(req, res)
    {
        if (!req.headers.privatekey)
            res.status(400).json({ error: 'Missing Private Key' });
        else
        {
            // get file from database
            const file = await CryptService.symmetricDecrypt(req.params.fileGuid, req.headers.privatekey, req.session.guid);

            if (file)
                res.status(200).send(file);
        }
    }
}

module.exports = CryptController;