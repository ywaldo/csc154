const UserService = require('../Services/UserService');

class UserController
{
    static async signIn(req, res)
    {
        const { username, password } = req.body;

        if (!username || !password)
            res.status(400).json({ message: 'Missing Username or Password' });
        else
        {
            const results = await UserService.signIn(username, password);

            if (results)
                res.status(200).json(results);
        }
    }

    static async signUp(req, res)
    {
        const { username, password } = req.body;

        if (!username || !password)
            res.status(400).json({ message: 'Missing Username or Password' });
        else
        {
            const data = await UserService.signUp(username, password);

            if (data)
                res.status(200).json(data);
        }
    }

    static async getUser(req, res)
    {
        const user = await UserService.get(req.session.guid);

        res.status(200).json(user);
    }

    static async updateUser(req, res)
    {
        await UserService.update(req.session.guid, req.body);

        res.status(200).json({ message: 'User Updated' });
    }

    static async deleteUser(req, res)
    {
        await UserService.deleteUser(req.session.guid);

        res.status(200).json({ message: 'User Deleted' });
    }

    static async getUserFiles(req, res)
    {
        const files = await UserService.getUserFiles(req.session.guid, req.query);

        res.status(200).json(files);
    }

    static async deleteFile(req, res)
    {
        await UserService.deleteAttachment(req.params.fileGuid, req.session.guid);

        res.status(200).json({ message: 'File Deleted' });
    }

    static async generateAsymKeyPair(req, res)
    {
        const keyPair = await UserService.generateAsymKeyPair(req.session.guid);

        res.status(200).json(keyPair);
    }

    static async generateHomKeyPair(req, res)
    {
        const keyPair = await UserService.generateHomKeyPair(req.session.guid);

        res.status(200).json(keyPair || {});
    }
}

module.exports = UserController;
