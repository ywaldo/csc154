const AttachmentService = require('../Services/AttachmentService');
const { DateTime } = require('luxon');

async function deleteOldAttachments()
{
    // get date of 15 minutes ago
    const date = DateTime.utc().minus({ minutes: 15 });

    // Get all attachments that are older than 15 minutes
    const attachments = await AttachmentService.search({ createdAt: { $lte: date.toString() } });

    // Delete all attachments
    await AttachmentService.bulkDelete(attachments);
}

module.exports = { deleteOldAttachments };