const cron = require('node-cron');
const Jobs = require('./Jobs');

// schedule task to run every 15 minutes
cron.schedule('*/15 * * * *', async () =>
{
    try
    {
        console.log('Running Cron Job To Delete Attachments');

        await Jobs.deleteOldAttachments();
    }
    catch (err)
    {
        console.log(err);
    }
});
