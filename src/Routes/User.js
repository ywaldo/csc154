const controller = require('../HttpControllers/UsersController');
const { uuidRegexStr } = require('../Utils/Regexes');
const router = require('express').Router();

const prefix = '/user'

router
    .post(`${prefix}/signin`, (req, res) => httpNoAuth(req, res, controller.signIn))
    .post(`${prefix}/signup`, (req, res) => httpNoAuth(req, res, controller.signUp))
    .get(`${prefix}`, (req, res) => http(req, res, controller.getUser))
    .patch(`${prefix}`, (req, res) => http(req, res, controller.updateUser))
    .delete(`${prefix}`, (req, res) => http(req, res, controller.deleteUser))
    .delete(`${prefix}/files/:fileGuid(${uuidRegexStr})`, (req, res) => http(req, res, controller.deleteFile))
    .get(`${prefix}/files`, (req, res) => http(req, res, controller.getUserFiles))
    .put(`${prefix}/generate/asymKeyPair`, (req, res) => http(req, res, controller.generateAsymKeyPair))
    .put(`${prefix}/generate/homKeyPair`, (req, res) => http(req, res, controller.generateHomKeyPair))

module.exports = router;