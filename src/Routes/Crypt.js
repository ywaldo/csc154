const CryptController = require('../HttpControllers/CryptController');
const { uuidRegexStr } = require('../Utils/Regexes');
const router = require('express').Router();
const multer = require('multer');

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });


router
    .post(`/encrypt/symmetric`, upload.single('file'), (req, res) => http(req, res, CryptController.encryptSymmetric))
    .get(`/decrypt/symmetric/:fileGuid(${uuidRegexStr})`, (req, res) => http(req, res, CryptController.decryptSymmetric))
    .post(`/encrypt/asymmetric`, upload.single('file'), (req, res) => http(req, res, CryptController.encryptAsymmetric))
    .get(`/decrypt/asymmetric/:fileGuid(${uuidRegexStr})`, (req, res) => http(req, res, CryptController.decryptAsymmetric))
    .post(`/encrypt/homomorphic`, upload.single('file'), (req, res) => http(req, res, CryptController.encryptHomomorphic))
    .get(`/decrypt/homomorphic/:fileGuid(${uuidRegexStr})`, (req, res) => http(req, res, CryptController.decryptHomomorphic))
    .post(`/homomorphic/add/:fileGuid(${uuidRegexStr})`, upload.single('file'), (req, res) => http(req, res, CryptController.homomorphicAddition))
    .post(`/homomorphic/multiply/:fileGuid(${uuidRegexStr})`, upload.single('file'), (req, res) => http(req, res, CryptController.homomorphicMultiplication))

module.exports = router;