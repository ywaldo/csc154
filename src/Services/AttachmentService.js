const AzureStorage = require('../Azure/Storage');
const Mongo = require('../Mongo/Client');
const { DateTime } = require('luxon');
const uuid = require('uuid');

const baseURL = AzureStorage.getBaseUrl();

class AttachmentService
{
    static async get(guid)
    {
        const attachment = await Mongo.findOne('attachments', { 'guid': guid });

        if (!attachment)
            throw { 'status': 400, 'data': 'Attachment Not Found' };

        // retrieve blob
        const data = await AzureStorage.getBlob(attachment.relativePath);

        return data;
    }

    static async search(filters)
    {
        const files = await Mongo.findAll('attachments', filters);

        // get sas
        const sas = AzureStorage.getSAS();

        // append security key
        for (const e of files)
            e.url += sas;

        return files;
    }

    static async insert(content, userGuid, opts)
    {
        // get sas
        const sas = AzureStorage.getSAS();

        // generate unique id
        const guid = uuid.v4();

        // compose full path
        const partialPath = `${userGuid}/${guid}/${opts.originalname}`;
        const fullPath = `${baseURL}/${partialPath}`;

        const file =
        {
            'guid': guid,
            'encType': opts.encType,
            'url': fullPath,
            'relativePath': partialPath,
            'name': opts.originalname,
            'owner': userGuid,
            'createdAt': DateTime.utc().toISO()
        };

        await Promise.all([AzureStorage.storeBlob(partialPath, content), Mongo.insert('attachments', file)]);

        // add sas to url
        file.url += sas;

        return file;
    }

    static async delete(guid)
    {
        // get blob
        const blob = await Mongo.findOne('attachments', { 'guid': guid });

        if (!blob)
            throw { 'status': 400, 'data': 'Attachment not found' };

        await Promise.all([Mongo.delete('attachments', { 'guid': guid }), AzureStorage.deleteBlob(blob.relativePath)]);
    }

    static async bulkDelete(arr)
    {
        const proms = [];

        for (const e of arr)
            proms.push({ func: AttachmentService.delete, params: e.guid });

        await Promise.allSettled(proms.map(e => e.func(e.params)));
    }

    static async update(fileGuid, content)
    {
        // get file from mongo
        const file = await Mongo.findOne('attachments', { 'guid': fileGuid });

        await AzureStorage.updateBlob(file.relativePath, content);
    }
}

module.exports = AttachmentService;