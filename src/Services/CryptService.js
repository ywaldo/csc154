const AttachmentService = require('./AttachmentService');
const Homomorphic = require('../Crypters/Homomorphic');
const Asymmetric = require('../Crypters/Asymmetric');
const Symmetric = require('../Crypters/Symmetric');
const UserService = require('./UserService');

class CryptService
{
    static async homomorphicAddition(fileGuid, file, userGuid)
    {
        // find file in storage and get user info
        const proms = await Promise.all([AttachmentService.get(fileGuid), UserService.getUserPrivate(userGuid)]);

        // save to variables
        const encrypted = proms[0];
        const { publicHomKey } = proms[1];

        // parse publicHomKey
        const [n, g] = publicHomKey.split(';');

        // restore public key
        const publicKey = Homomorphic.restorePublicKey(n, g);

        // int from buffer
        const int = parseInt(file.buffer.toString('utf8'));

        // encrypt int
        const encryptedInt = BigInt(Homomorphic.encrypt(publicKey, int));

        // add encrypted content
        const added = Homomorphic.add(encrypted, encryptedInt, publicKey);

        // update file
        await AttachmentService.update(fileGuid, added.toString());

        return { 'data': 'Added Successfully' };
    }

    static async homomorphicMultiplication(fileGuid, file, userGuid)
    {
        // find file in storage and get user info
        const proms = await Promise.all([AttachmentService.get(fileGuid), UserService.getUserPrivate(userGuid)]);

        // save to variables
        const encrypted = proms[0];
        const { publicHomKey } = proms[1];

        // parse publicHomKey
        const [n, g] = publicHomKey.split(';');

        // restore public key
        const publicKey = Homomorphic.restorePublicKey(n, g);

        // int from buffer
        const int = parseInt(file.buffer.toString('utf8'));

        // multiply encrypted content
        const multiplied = Homomorphic.multiply(encrypted, int, publicKey);

        // update file
        await AttachmentService.update(fileGuid, multiplied.toString());

        return { 'data': 'Multiplied Successfully' };
    }

    static async homomorphicEncrypt(file, userGuid)
    {
        // get user info
        const user = await UserService.getUserPrivate(userGuid);

        if (!user.publicHomKey)
            throw { 'error': 'Key Pair Not Generated Yet' };

        // parse publicHomKey
        const [n, g] = user.publicHomKey.split(';');

        // restore public key
        const publicKey = Homomorphic.restorePublicKey(n, g);

        // convert buffer to string to int
        const int = parseInt(file.buffer.toString('utf8'));

        // encrypt content of file
        const encrypted = Homomorphic.encrypt(publicKey, int);

        // set enc type
        file.encType = 'homomorphic';

        // upload encrypted content to storage
        const attachment = await AttachmentService.insert(encrypted, userGuid, file)

        return attachment;
    }

    static async homomorphicDecrypt(fileGuid, key, userGuid)
    {
        // get user and attatchment
        const proms = await Promise.all([UserService.getUserPrivate(userGuid), AttachmentService.get(fileGuid)]);

        // save to variables
        const { publicHomKey } = proms[0];
        const encrypted = proms[1];

        // parse publicHomKey
        const [n, g] = publicHomKey.split(';');

        // parse private key
        const [lambda, mu] = key.split(';');

        // restore public key
        const publicKey = Homomorphic.restorePublicKey(n, g);

        // restore private key
        const privateKey = Homomorphic.restorePrivateKey(lambda, mu, publicKey);

        // decrypt content of file
        const decrypted = Homomorphic.decrypt(privateKey, encrypted);

        return decrypted.toString();
    }

    static generateNewHomormophicKeyPair()
    {
        const keyPair = Homomorphic.generateKeyPair();

        return keyPair;
    }

    static async asymmetricEncrypt(file, userGuid)
    {
        // get user
        const user = await UserService.getUserPrivate(userGuid);

        if (!user.publicAsymKey)
            throw { 'error': 'Key Pair Not Generated Yet' };

        // encrypt content of file
        const encrypted = Asymmetric.encrypt(file.buffer, user.publicAsymKey);

        // set enc type
        file.encType = 'asymmetric';

        // upload encrypted content to storage
        const attachment = await AttachmentService.insert(encrypted, userGuid, file)

        return attachment;
    }

    static async asymmetricDecrypt(fileGuid, key, userGuid)
    {
        // get encrypted content from storage
        const encrypted = await AttachmentService.get(fileGuid);

        // decrypt content of file
        const decrypted = Asymmetric.decrypt(encrypted, key, userGuid);

        return decrypted;
    }

    static generateKeyPair(guid)
    {
        const keyPair = Asymmetric.generateKeyPair(guid);

        return keyPair;
    }

    static async symmetricEncrypt(file, key, userGuid)
    {
        // get user iv and key
        const user = await UserService.getUserPrivate(userGuid);

        // encrypt content of file
        const encrypted = Symmetric.encrypt(file.buffer, key, { iv: user.privateIv, salt: user.privateSalt });

        // set enc type
        file.encType = 'symmetric';

        // upload encrypted content to storage
        const attachment = await AttachmentService.insert(encrypted, userGuid, file)

        return attachment;
    }

    static async symmetricDecrypt(fileGuid, key, userGuid)
    {
        // get user iv and key
        const user = await UserService.getUserPrivate(userGuid);

        // get encrypted content from storage
        const encrypted = await AttachmentService.get(fileGuid);

        // decrypt content of file
        const decrypted = Symmetric.decrypt(encrypted, key, { iv: user.privateIv, salt: user.privateSalt });

        return decrypted;
    }
}

module.exports = CryptService;