const jwt = require('jsonwebtoken');

const secret = process.env['jwt.secret'];

class AuthService
{
    static generateToken(data)
    {
        return jwt.sign(data, secret, { expiresIn: '1h' });
    }

    static verifyToken(token)
    {
        try
        {
            if (!token)
                throw new Error('Token not provided');

            // split token into parts
            const parts = token.split(' ');

            return jwt.verify(parts[1], secret);
        }
        catch (err)
        {
            throw { 'status': 401, 'data': 'Invalid token' };
        }
    }
}

module.exports = AuthService;