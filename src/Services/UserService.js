const AttachmentService = require('./AttachmentService');
const AuthService = require('./AuthService');
const Mongo = require('../Mongo/Client');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uuid = require('uuid').v4;

const salt = process.env['hashing.salt']

// user fields to ommit
const omitFields = { password: 0, __v: 0, privateIv: 0, privateSalt: 0 };

class UserService
{
    // method to sign up
    static async signUp(username, password)
    {
        // check if user already exists
        const user = await Mongo.findOne('users', { username });

        if (!user)
        {
            // encrypt password
            const hashedPassword = await bcrypt.hash(password, salt);

            const payload = {
                username,
                password: hashedPassword,
                guid: uuid(),
                privateIv: crypto.randomBytes(16).toString('hex'),
                privateSalt: crypto.randomBytes(16).toString('hex'),
            }

            // create new user
            await Mongo.insert('users', payload);

            // create token
            const token = AuthService.generateToken({ guid: payload.guid });

            // return user
            return { 'access_token': token };
        }
        else
            throw { 'status': 400, 'data': 'Username Already In Use' };
    }

    // method to sign in
    static async signIn(username, password)
    {
        // find user
        const user = await Mongo.findOne('users', { username });

        if (user)
        {
            // compare password
            const isValid = await bcrypt.compare(password, user.password);

            if (isValid)
            {
                // create token
                const token = AuthService.generateToken({ guid: user.guid });

                // return user
                return { 'access_token': token };
            }
        }
        else
            throw { 'status': 401, 'data': 'Invalid Password Or Username' };
    }

    static async get(guid)
    {
        const user = await Mongo.findOne('users', { guid }, omitFields);

        return user;
    }

    // meant for internal use only, should not be called from outside
    static async getUserPrivate(guid)
    {
        const user = await Mongo.findOne('users', { guid })

        return user;
    }

    static async update(guid, payload)
    {
        await Mongo.upsert('users', { guid }, payload);
    }

    static async deleteUser(guid)
    {
        // get all attachments for user
        const files = await UserService.getUserFiles(guid);

        // delete all attachments
        for (const file of files)
            await AttachmentService.delete(file.guid)

        // delete user
        await Mongo.delete('users', { guid });

        return true;
    }

    static async deleteAttachment(guid)
    {
        await AttachmentService.delete(guid);

        return true;
    }

    static async getUserFiles(guid, filter = undefined)
    {
        let q = { owner: guid }
        if (filter?.name)
            q = Object.assign({ name: new RegExp(filter.name) }, q);

        const files = await AttachmentService.search(q);

        return files;
    }

    static async generateAsymKeyPair(guid)
    {
        //requiring it here to avoid circular dependency
        const CryptService = require('./CryptService');

        // generate new key pair
        const keyPair = CryptService.generateKeyPair(guid);

        // update user
        await Mongo.upsert('users', { guid }, { publicAsymKey: keyPair.publicKey });

        return keyPair;
    }

    static async generateHomKeyPair(guid)
    {
        //requiring it here to avoid circular dependency
        const CryptService = require('./CryptService');

        // generate new key pair
        const { publicKey, privateKey } = CryptService.generateNewHomormophicKeyPair();

        // save n, g, lambda, mu
        const publicKeyStr = publicKey.n + ';' + publicKey.g;
        const privateKeyStr = privateKey.lambda + ';' + privateKey.mu;

        // update user
        await Mongo.upsert('users', { guid }, { publicHomKey: publicKeyStr });

        return { publicKey: publicKeyStr, privateKey: privateKeyStr };
    }
}

module.exports = UserService;